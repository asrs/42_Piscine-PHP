<?php
if (isset($_POST["submit"]) && $_POST["submit"] == "OK")
{
	if ($_POST["login"] != "" && $_POST["passwd"] != "")
	{
		$pss = hash("sha256", $_POST["passwd"]);
		$tab = array(array("login"=>$_POST["login"], "passwd"=>$pss));
		if (file_exists("../private") == FALSE)
			mkdir("../private", 0777, true);
		if (file_exists("../private/passwd") == FALSE)
		{
			$process = serialize($tab);
			file_put_contents("../private/passwd", $process);
			echo "OK\n";
		}
		else
		{
			$taken = FALSE;
			$file = file_get_contents("../private/passwd");
			$data = unserialize($file);
			foreach($data as $tmp)
			{
				if ($tmp["login"] == $_POST["login"])
					$taken = TRUE;
			}
			if ($taken == FALSE)
			{
				$data[] = array("login" => $_POST["login"], "passwd" => $pss);
				$process = serialize($data);
				file_put_contents("../private/passwd", $process);
				echo "OK\n";
			}
			else
				echo "ERROR\n";
		}
	}
	else
		echo "ERROR\n";
}
else
	echo "ERROR\n";
?>
