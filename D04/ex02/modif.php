<?php
if (isset($_POST["submit"]) && $_POST["submit"] == "OK")
{
	if ($_POST["login"] != "" && $_POST["oldpw"] != "" && $_POST["newpw"] != "")
	{
		$i = 0;
		$modif = false;
		$old = hash("sha256", $_POST["oldpw"]);
		$new = hash("sha256", $_POST["newpw"]);
		$file = file_get_contents("../private/passwd");
		$data = unserialize($file);
		foreach($data as $tmp)
		{
			if ($tmp["login"] === $_POST["login"])
			{
				$modif = true;
				if ($tmp["passwd"] === $old)
					$data[$i]["passwd"] = $new;
				else
					$modif = false;
				break;
			}
			$i++;
		}
		if ($modif == true)
		{
			$process = serialize($data);
			file_put_contents("../private/passwd", $process);
			echo "OK\n";
		}
		else
			echo "ERROR\n";
	}
	else
		echo "ERROR\n";
}
else
	echo "ERROR\n";
?>
