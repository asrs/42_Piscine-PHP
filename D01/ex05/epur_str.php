#!/usr/bin/php
<?php
$i = 0;
if ($argc != 2)
	exit();
$s = trim($argv[1]);
if (strpos($s, ' ') == TRUE)
{
	$tab = explode(" ", $s);
	$len = count($tab);
	while ($i < ($len - 1))
	{
		if ($tab[$i] != "")
		{
			print($tab[$i]);
			print(" ");
		}
		$i++;
	}
	print($tab[$i]);
	print("\n");
}
else
{
	print($s);
	print("\n");
}
?>
