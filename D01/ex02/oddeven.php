#!/usr/bin/php
<?php
while (1)
{
	print("Entrez un nombre: ");
	$line = trim(fgets(STDIN));
	if (feof(STDIN) == true)
		exit();
	if (!is_numeric($line))
	{
		print("'$line'");
		print(" n'est pas un chiffre\n");
	}
	else
	{
		if ($line % 2 == 0)
			print("Le nombre $line est Pair\n");
		else
			print("Le nombre $line est Impair\n");
	}
}
?>
