#!/usr/bin/php
<?php
function ft_split($s)
{
	$tab = explode(" ", $s);
	sort($tab);
	return($tab);
}

function puttab($array)
{
	$i = 0;
	$len = count($array);
	while ($i < $len)
	{
		print($array[$i]);
		print("\n");
		$i++;
	}
}

function is_alpha($string)
{
	$i = 0;
	while ($string[$i])
	{
		if (!(($string[$i] >= 'a' && $string[$i] <= 'z') || $string[$i] >='A' &&
		$string[$i] <= 'Z'))
			return (FALSE);
		$i++;
	}
	return (TRUE);
}

$i = 1;
$ret = array();
if ($argc < 2)
	exit();
while ($i < $argc)
{
	$tab = ft_split($argv[$i]);
	$ret = array_merge($ret, $tab);
	$i++;
}
$i = 0;
$len = count($ret);
$e = array();
$num = array();
$string = array();
$ascii = array();
while ($i < $len)
{
	$e[0] = $ret[$i];
	if (is_numeric($ret[$i]) == TRUE)
		$num = array_merge($num, $e);
	else if (is_alpha($ret[$i]) == TRUE)
		$string = array_merge($string, $e);
	else if (is_alpha($ret[$i]) == FALSE && is_numeric($ret[$i]) == FALSE)
		$ascii = array_merge($ascii, $e);
	$i++;
}
sort($num, SORT_STRING);
sort($string, SORT_NATURAL | SORT_FLAG_CASE);
sort($ascii);
puttab($string);
puttab($num);
puttab($ascii);
?>
