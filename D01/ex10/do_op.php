#!/usr/bin/php
<?php
if ($argc == 4)
{
	$n1 = trim($argv[1]);
	$op = trim($argv[2]);
	$n2 = trim($argv[3]);

	if ($op == "/")
		$result = $n1 / $n2;
	else if ($op == "*")
		$result = $n1 * $n2;
	else if ($op == "-")
		$result = $n1 - $n2;
	else if ($op == "%")
		$result = $n1 % $n2;
	else if ($op == "+")
		$result = $n1 + $n2;
	print($result);
	print("\n");
}
else
	print("Incorrect Parameters\n");
?>
